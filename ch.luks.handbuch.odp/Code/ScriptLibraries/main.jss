function amIResponsible( workers) {
	if (workers == null) {
		return true;
	}
	var myRolesAndGroups = xptBean.getMyGroupsAndRoles();
	for (var counter = 0; counter < workers.size(); counter++) {
		var worker = workers.get(counter);
		if (myRolesAndGroups.contains( worker )) {
			return true;
		}
	}
	return false;
}