function toggleNavigation(nodeId) {
	if (toggled != "" && toggled != nodeId) {
		toggleNavigation(toggled);
	}
	var element = dojo.byId(nodeId);
	var wipeArgs = {
		node : nodeId
	};
	if (element.style.display == 'none') {
		dojo.fx.wipeIn(wipeArgs).play();
		toggled = nodeId;
	} else {
		dojo.fx.wipeOut(wipeArgs).play();
		toggled = "";
	}
	
}